#!/bin/sh
# TODO: not use external command
# TODO: error check
#---------------------------------------
# apache chared memory size check
#---------------------------------------
echo '------------------------------'
echo -e "PID\tETIME\tRSS(kB)\tSHARED(kB)"
echo '------------------------------'
ps -eo pid,ppid,etime,cmd | grep http | grep -v grep | while read line
do
  read -r pid ppid etime cmd <<< $line
  execmd="/proc/${pid}/status"
  if [ -a ${execmd} -a ${ppid} -ne 1 ]; then
    VmHWM=`cat ${execmd} | grep VmHWM | awk '{print $2}'`
    execmd2="/proc/${pid}/smaps"
    if [ -a ${execmd2} ]; then
      SHARED=`cat ${execmd2} | grep -E "Shared_Clean|Shared_Dirty" | awk '{shared +=$2} END {print shared}'`
      per=`echo "scale=2; ${SHARED}/${VmHWM}" | bc`
      per=`echo "${per} * 100" | bc`
    fi
    echo -e "${pid}\t${etime}\t${VmHWM}\t${SHARED} (${per%.*}%)"
  fi
done
